/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.blois.siad.jee.projet.entite;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * @author ahmed
 */
@Entity
@Table(name = "PRODUIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = Produit.PRODUIT_FIND_ALL, query = "SELECT p FROM Produit p"),
    @NamedQuery(name = Produit.PRODUIT_FIND_BY_PRD_ID, query = "SELECT p FROM Produit p WHERE p.prdId = :" + Produit.PRODUIT_ID),
    @NamedQuery(name = Produit.PRODUIT_FIND_BY_PRD_LIBELLE, query = "SELECT p FROM Produit p WHERE p.prdLibelle = :prdLibelle"),
    @NamedQuery(name = Produit.PRODUIT_FIND_BY_PRD_PRIX, query = "SELECT p FROM Produit p WHERE p.prdPrix = :prdPrix")
})
public class Produit implements Serializable {

  private static final long serialVersionUID = 1L;
  public static final String PRODUIT_FIND_ALL = "Produit.findAll";
  public static final String PRODUIT_FIND_BY_PRD_ID = "Produit.findByPrdId";
  public static final String PRODUIT_FIND_BY_PRD_LIBELLE = "Produit.findByPrdLibelle";
  public static final String PRODUIT_FIND_BY_PRD_PRIX = "Produit.findByPrdPrix";
  public static final String PRODUIT_ID = "prdId";

  @Id
  @Basic(optional = false)
  @Column(name = "PRD_ID")
  private Integer prdId;

  @Basic(optional = false)
  @Column(name = "PRD_LIBELLE", length = 100)
  private String prdLibelle;

  @Basic(optional = false)
  @Column(name = "PRD_PRIX")
  private double prdPrix;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "produit")
  private List<Produitsvente> produitsventeList;

  public Produit() {
  }

  public Produit(Integer prdId) {
    this.prdId = prdId;
  }

  public Produit(Integer prdId, String prdLibelle, double prdPrix) {
    this.prdId = prdId;
    this.prdLibelle = prdLibelle;
    this.prdPrix = prdPrix;
  }

  public Integer getPrdId() {
    return prdId;
  }

  public void setPrdId(Integer prdId) {
    this.prdId = prdId;
  }

  public String getPrdLibelle() {
    return prdLibelle;
  }

  public void setPrdLibelle(String prdLibelle) {
    this.prdLibelle = prdLibelle;
  }

  public double getPrdPrix() {
    return prdPrix;
  }

  public void setPrdPrix(double prdPrix) {
    this.prdPrix = prdPrix;
  }

  @XmlTransient
  public List<Produitsvente> getProduitsventeList() {
    return produitsventeList;
  }

  public void setProduitsventeList(List<Produitsvente> produitsventeList) {
    this.produitsventeList = produitsventeList;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (prdId != null ? prdId.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Produit)) {
      return false;
    }
    Produit other = (Produit) object;
    if ((this.prdId == null && other.prdId != null) || (this.prdId != null && !this.prdId.equals(other.prdId))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Produit{" +
        "prdId=" + prdId +
        ", prdLibelle='" + prdLibelle + '\'' +
        ", prdPrix=" + prdPrix +
        ", produitsventeList=" + produitsventeList +
        '}';
  }

}
