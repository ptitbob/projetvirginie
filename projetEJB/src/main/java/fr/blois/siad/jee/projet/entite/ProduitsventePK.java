/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.blois.siad.jee.projet.entite;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;


/**
 * @author ahmed
 */
@Embeddable
public class ProduitsventePK implements Serializable {

  @Basic(optional = false)
  @Column(name = "VPD_ID_VENTE")
  private int vpdIdVente;

  @Basic(optional = false)
  @Column(name = "VPD_ID_PRODUIT")
  private int vpdIdProduit;

  public ProduitsventePK() {
  }

  public ProduitsventePK(int vpdIdVente, int vpdIdProduit) {
    this.vpdIdVente = vpdIdVente;
    this.vpdIdProduit = vpdIdProduit;
  }

  public int getVpdIdVente() {
    return vpdIdVente;
  }

  public void setVpdIdVente(int vpdIdVente) {
    this.vpdIdVente = vpdIdVente;
  }

  public int getVpdIdProduit() {
    return vpdIdProduit;
  }

  public void setVpdIdProduit(int vpdIdProduit) {
    this.vpdIdProduit = vpdIdProduit;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof ProduitsventePK)) return false;
    ProduitsventePK that = (ProduitsventePK) o;
    return getVpdIdVente() == that.getVpdIdVente() &&
        getVpdIdProduit() == that.getVpdIdProduit();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getVpdIdVente(), getVpdIdProduit());
  }

  @Override
  public String toString() {
    return "ProduitsventePK{" +
        "vpdIdProduit=" + vpdIdProduit +
        ", vpdIdVente=" + vpdIdVente +
        '}';
  }

}
