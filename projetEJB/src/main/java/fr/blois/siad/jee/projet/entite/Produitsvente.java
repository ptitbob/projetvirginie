/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.blois.siad.jee.projet.entite;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author ahmed
 */
@Entity
@Table(name = "PRODUITSVENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = Produitsvente.PRODUITSVENTE_FIND_ALL, query = "SELECT p FROM Produitsvente p"),
    @NamedQuery(name = Produitsvente.PRODUITSVENTE_FIND_BY_VPD_ID_VENTE, query = "SELECT p FROM Produitsvente p WHERE p.produitsventePK.vpdIdVente = :vpdIdVente"),
    @NamedQuery(name = Produitsvente.PRODUITSVENTE_FIND_BY_VPD_ID_PRODUIT, query = "SELECT p FROM Produitsvente p WHERE p.produitsventePK.vpdIdProduit = :vpdIdProduit"),
    @NamedQuery(name = Produitsvente.PRODUITSVENTE_FIND_BY_VPD_QUANTITE, query = "SELECT p FROM Produitsvente p WHERE p.vpdQuantite = :vpdQuantite")
})
public class Produitsvente implements Serializable {

  private static final long serialVersionUID = 1L;
  public static final String PRODUITSVENTE_FIND_ALL = "Produitsvente.findAll";
  public static final String PRODUITSVENTE_FIND_BY_VPD_ID_VENTE = "Produitsvente.findByVpdIdVente";
  public static final String PRODUITSVENTE_FIND_BY_VPD_ID_PRODUIT = "Produitsvente.findByVpdIdProduit";
  public static final String PRODUITSVENTE_FIND_BY_VPD_QUANTITE = "Produitsvente.findByVpdQuantite";

  @EmbeddedId
  protected ProduitsventePK produitsventePK;

  @Basic(optional = false)
  @Column(name = "VPD_QUANTITE")
  private int vpdQuantite;

  @JoinColumn(name = "VPD_ID_VENTE", referencedColumnName = "VEN_ID", insertable = false, updatable = false)
  @ManyToOne(optional = false)
  private Vente vente;

  @JoinColumn(name = "VPD_ID_PRODUIT", referencedColumnName = "PRD_ID", insertable = false, updatable = false)
  @ManyToOne(optional = false)
  private Produit produit;

  public Produitsvente() {
  }

  public Produitsvente(ProduitsventePK produitsventePK) {
    this.produitsventePK = produitsventePK;
  }

  public Produitsvente(ProduitsventePK produitsventePK, int vpdQuantite) {
    this.produitsventePK = produitsventePK;
    this.vpdQuantite = vpdQuantite;
  }

  public Produitsvente(int vpdIdVente, int vpdIdProduit) {
    this.produitsventePK = new ProduitsventePK(vpdIdVente, vpdIdProduit);
  }

  public ProduitsventePK getProduitsventePK() {
    return produitsventePK;
  }

  public void setProduitsventePK(ProduitsventePK produitsventePK) {
    this.produitsventePK = produitsventePK;
  }

  public int getVpdQuantite() {
    return vpdQuantite;
  }

  public void setVpdQuantite(int vpdQuantite) {
    this.vpdQuantite = vpdQuantite;
  }

  public Vente getVente() {
    return vente;
  }

  public void setVente(Vente vente) {
    this.vente = vente;
  }

  public Produit getProduit() {
    return produit;
  }

  public void setProduit(Produit produit) {
    this.produit = produit;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (produitsventePK != null ? produitsventePK.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Produitsvente)) {
      return false;
    }
    Produitsvente other = (Produitsvente) object;
    if ((this.produitsventePK == null && other.produitsventePK != null) || (this.produitsventePK != null && !this.produitsventePK.equals(other.produitsventePK))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Produitsvente{" +
        "produit=" + produit +
        ", produitsventePK=" + produitsventePK +
        ", vpdQuantite=" + vpdQuantite +
        ", vente=" + vente +
        '}';
  }

}
