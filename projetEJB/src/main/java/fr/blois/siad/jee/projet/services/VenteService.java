package fr.blois.siad.jee.projet.services;

import fr.blois.siad.jee.projet.entite.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 *
 */
@Stateless
public class VenteService {

  private static EntityManagerFactory entityManagerFactory;

  private EntityManager entityManager;

  @EJB
  private ProduitService produitService;

  @PostConstruct
  public void initialize() {
    entityManagerFactory = Persistence.createEntityManagerFactory("Projet");
    entityManager = entityManagerFactory.createEntityManager();
  }

  public List<Vente> getDerniereVenteList() {
    return entityManager.createNamedQuery(Vente.VENTE_FIND_ALL_INVERSE, Vente.class)
        .getResultList();
  }

  /**
   * La base étant bloqué (création indépendante), nous avons réalisé une gestion interne des id. Cependant le mieux serai une gestion par séquence.
   *
   * @return nouvel identifiant libre
   */
  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public int getNouveauVenteId() {
    Integer nouvelId = entityManager.createNamedQuery(Vente.VENTE_FIND_MAX_ID, Integer.class)
        .getSingleResult();
    if (nouvelId == null) {
      return 0;
    } else {
      return nouvelId + 1;
    }
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public Vente nouvelleVente() {
    Vente vente = new Vente();
    vente.setVenId(getNouveauVenteId());
    vente.setVenDtheure(new Date());
    vente.setVenIdEtat(getEtat(1));
    vente.setProduitsventeList(new ArrayList<Produitsvente>());
    vente.setVenPrixbrut(0D);
    vente.setVenPrctremise(0D);
    vente.setVenPrixnet(0D);
    /* comme nous utilisons une ressource jdbc créée manuellement et non un pool de connexion géré par le serveur, nous devons gérer la transaction */
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityTransaction.begin();
    entityManager.persist(vente);
    entityTransaction.commit();
    return vente;
  }

  /**
   * Renvoi l'etat de la vente selon l'Id
   *
   * @param etatVenteId id de l'etat
   * @return etat de la vente
   */
  private Etatvente getEtat(Integer etatVenteId) {
    return entityManager.createNamedQuery(Etatvente.ETATVENTE_FIND_BY_ETV_ID, Etatvente.class)
        .setParameter(Etatvente.ETATVENTE_ID, etatVenteId)
        .getSingleResult();
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public void ajouterProduit(Vente vente, Integer produitId) {
        /* comme nous utilisons une ressource jdbc créée manuellement et non un pool de connexion géré par le serveur, nous devons gérer la transaction */
    Produit produit = produitService.getProduit(produitId);
    Produitsvente produitsvente = getProduitVente(vente.getProduitsventeList(), produit);
    if (produitsvente == null) {
      vente.getProduitsventeList().add(creerProduitVente(vente, produit));
    } else {
      ajouteQuantiteProduit(produitsvente);
    }
    calculPrix(vente);
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityTransaction.begin();
    entityManager.merge(vente); // on ramene l'entité dans le contexte de EntityManager et on la sauvegarde
    entityTransaction.commit();
  }

  private void calculPrix(Vente vente) {
    Double prix = 0D;
    for (Produitsvente produitsventeDansListe : vente.getProduitsventeList()) {
      prix = prix + (produitsventeDansListe.getProduit().getPrdPrix() * produitsventeDansListe.getVpdQuantite());
    }
    vente.setVenPrixbrut(prix);
    vente.setVenPrixnet(vente.getVenPrixbrut() - vente.getVenPrctremise());
    if (vente.getVenPrixnet() < 0) {
      vente.setVenPrixnet(0D);
    }
  }

  private void ajouteQuantiteProduit(Produitsvente produitsvente) {
    produitsvente.setVpdQuantite(produitsvente.getVpdQuantite() + 1);
        /* comme nous utilisons une ressource jdbc créée manuellement et non un pool de connexion géré par le serveur, nous devons gérer la transaction */
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityTransaction.begin();
    entityManager.merge(produitsvente);
    entityTransaction.commit();
  }

  private Produitsvente creerProduitVente(Vente vente, Produit produit) {
    Produitsvente produitsvente = new Produitsvente();
    produitsvente.setProduitsventePK(new ProduitsventePK(vente.getVenId(), produit.getPrdId()));
    produitsvente.setVpdQuantite(1);
    produitsvente.setVente(vente);
    produitsvente.setProduit(produit);
    /* comme nous utilisons une ressource jdbc créée manuellement et non un pool de connexion géré par le serveur, nous devons gérer la transaction */
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityTransaction.begin();
    entityManager.persist(produitsvente);
    entityTransaction.commit();
    return produitsvente;
  }

  private Produitsvente getProduitVente(List<Produitsvente> produitsventeList, Produit produit) {
    if (produitsventeList == null) {
      return null;
    }
    Produitsvente produitsvente = null;
    Iterator<Produitsvente> produitsventeIterator = produitsventeList.iterator();
    while (produitsventeIterator.hasNext() && produitsvente == null) {
      produitsvente = produitsventeIterator.next();
      if (!produitsvente.getProduit().equals(produit)) {
        produitsvente = null;
      }
    }
    return produitsvente;
  }

  public void appliquerRemise(Vente vente) {
    calculPrix(vente);
    /* comme nous utilisons une ressource jdbc créée manuellement et non un pool de connexion géré par le serveur, nous devons gérer la transaction */
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityTransaction.begin();
    entityManager.merge(vente);
    entityTransaction.commit();
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public void validerVente(Vente vente) {
    changeEtatVenteEtPersiste(vente, 2);
  }

  private void changeEtatVenteEtPersiste(Vente vente, int etatVenteId) {
    calculPrix(vente);
    vente.setVenIdEtat(getEtat(etatVenteId));
    /* comme nous utilisons une ressource jdbc créée manuellement et non un pool de connexion géré par le serveur, nous devons gérer la transaction */
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityTransaction.begin();
    entityManager.merge(vente);
    entityTransaction.commit();
  }

  public void annuleVente(Vente vente) {
    changeEtatVenteEtPersiste(vente, 3);
  }
}
