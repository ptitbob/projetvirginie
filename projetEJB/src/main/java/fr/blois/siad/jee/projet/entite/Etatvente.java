/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.blois.siad.jee.projet.entite;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * @author ahmed
 */
@Entity
@Table(name = "ETATVENTE")
@NamedQueries({
    @NamedQuery(name = Etatvente.ETATVENTE_FIND_ALL, query = "SELECT e FROM Etatvente e"),
    @NamedQuery(name = Etatvente.ETATVENTE_FIND_BY_ETV_ID, query = "SELECT e FROM Etatvente e WHERE e.etvId = :" + Etatvente.ETATVENTE_ID),
    @NamedQuery(name = Etatvente.ETATVENTE_FIND_BY_ETV_LIBELLE, query = "SELECT e FROM Etatvente e WHERE e.etvLibelle = :etvLibelle")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Etatvente implements Serializable {

  private static final long serialVersionUID = 1L;
  public static final String ETATVENTE_FIND_ALL = "Etatvente.findAll";
  public static final String ETATVENTE_FIND_BY_ETV_ID = "Etatvente.findByEtvId";
  public static final String ETATVENTE_FIND_BY_ETV_LIBELLE = "Etatvente.findByEtvLibelle";
  public static final String ETATVENTE_ID = "etvId";

  @Id
  @Basic(optional = false)
  @Column(name = "ETV_ID")
  private Integer etvId;

  @Basic(optional = false)
  @Column(name = "ETV_LIBELLE", length = 100)
  private String etvLibelle;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "venIdEtat")
  private List<Vente> venteList;

  /**
   * Constructeur
   */
  public Etatvente() {
  }

  public Etatvente(Integer etvId) {
    this.etvId = etvId;
  }

  public Etatvente(Integer etvId, String etvLibelle) {
    this.etvId = etvId;
    this.etvLibelle = etvLibelle;
  }

  public Integer getEtvId() {
    return etvId;
  }

  public void setEtvId(Integer etvId) {
    this.etvId = etvId;
  }

  public String getEtvLibelle() {
    return etvLibelle;
  }

  public void setEtvLibelle(String etvLibelle) {
    this.etvLibelle = etvLibelle;
  }

  @XmlTransient
  public List<Vente> getVenteList() {
    return venteList;
  }

  public void setVenteList(List<Vente> venteList) {
    this.venteList = venteList;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (etvId != null ? etvId.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Etatvente)) {
      return false;
    }
    Etatvente other = (Etatvente) object;
    if ((this.etvId == null && other.etvId != null) || (this.etvId != null && !this.etvId.equals(other.etvId))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Etatvente{" +
        "etvId=" + etvId +
        ", etvLibelle='" + etvLibelle + '\'' +
        ", venteList=" + venteList +
        '}';
  }

}
