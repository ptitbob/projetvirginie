/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.blois.siad.jee.projet.entite;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author ahmed
 */
@Entity
@Table(name = "TYPEPAIEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = Typepaiement.TYPEPAIEMENT_FIND_ALL, query = "SELECT t FROM Typepaiement t"),
    @NamedQuery(name = Typepaiement.TYPEPAIEMENT_FIND_BY_TYP_ID, query = "SELECT t FROM Typepaiement t WHERE t.typId = :typId"),
    @NamedQuery(name = Typepaiement.TYPEPAIEMENT_FIND_BY_TYP_LIBELLE, query = "SELECT t FROM Typepaiement t WHERE t.typLibelle = :typLibelle")
})
public class Typepaiement implements Serializable {

  private static final long serialVersionUID = 1L;
  public static final String TYPEPAIEMENT_FIND_ALL = "Typepaiement.findAll";
  public static final String TYPEPAIEMENT_FIND_BY_TYP_ID = "Typepaiement.findByTypId";
  public static final String TYPEPAIEMENT_FIND_BY_TYP_LIBELLE = "Typepaiement.findByTypLibelle";

  @Id
  @Basic(optional = false)
  @Column(name = "TYP_ID")
  private Integer typId;

  @Basic(optional = false)
  @Column(name = "TYP_LIBELLE", length = 100)
  private String typLibelle;

  @OneToMany(mappedBy = "venIdTypePaiement")
  private List<Vente> venteList;

  public Typepaiement() {
  }

  public Typepaiement(Integer typId) {
    this.typId = typId;
  }

  public Typepaiement(Integer typId, String typLibelle) {
    this.typId = typId;
    this.typLibelle = typLibelle;
  }

  public Integer getTypId() {
    return typId;
  }

  public void setTypId(Integer typId) {
    this.typId = typId;
  }

  public String getTypLibelle() {
    return typLibelle;
  }

  public void setTypLibelle(String typLibelle) {
    this.typLibelle = typLibelle;
  }

  @XmlTransient
  public List<Vente> getVenteList() {
    return venteList;
  }

  public void setVenteList(List<Vente> venteList) {
    this.venteList = venteList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Typepaiement)) return false;
    Typepaiement that = (Typepaiement) o;
    return Objects.equals(getTypId(), that.getTypId()) &&
        Objects.equals(getTypLibelle(), that.getTypLibelle()) &&
        Objects.equals(getVenteList(), that.getVenteList());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getTypId(), getTypLibelle(), getVenteList());
  }

  @Override
  public String toString() {
    return "Typepaiement{" +
        "typId=" + typId +
        ", typLibelle='" + typLibelle + '\'' +
        ", venteList=" + venteList +
        '}';
  }

}
