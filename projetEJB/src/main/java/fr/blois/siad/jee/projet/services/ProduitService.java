package fr.blois.siad.jee.projet.services;

import fr.blois.siad.jee.projet.entite.Produit;

import javax.annotation.PostConstruct;
import javax.ejb.BeforeCompletion;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 *
 *
 */
@Stateless
public class ProduitService {

  private static EntityManagerFactory entityManagerFactory;

  private EntityManager entityManager;

  @PostConstruct
  public void initialize() {
    entityManagerFactory = Persistence.createEntityManagerFactory("Projet");
    entityManager = entityManagerFactory.createEntityManager();
  }

  @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
  public List<Produit> getProduitList() {
    return entityManager.createNamedQuery(Produit.PRODUIT_FIND_ALL, Produit.class)
        .getResultList();
  }

  public Produit getProduit(Integer produitId) {
    return entityManager.createNamedQuery(Produit.PRODUIT_FIND_BY_PRD_ID, Produit.class)
        .setParameter(Produit.PRODUIT_ID, produitId)
        .getSingleResult();
  }
}
