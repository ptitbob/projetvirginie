/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.blois.siad.jee.projet.entite;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author ahmed
 */
@Entity
@Table(name = "VENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = Vente.VENTE_FIND_ALL, query = "SELECT v FROM Vente v"),
    @NamedQuery(name = Vente.VENTE_FIND_ALL_INVERSE, query = "select v from Vente v order by v.venDtheure DESC"),
    @NamedQuery(name = Vente.VENTE_FIND_BY_VEN_ID, query = "SELECT v FROM Vente v WHERE v.venId = :venId"),
    @NamedQuery(name = Vente.VENTE_FIND_BY_VEN_DTHEURE, query = "SELECT v FROM Vente v WHERE v.venDtheure = :venDtheure"),
    @NamedQuery(name = Vente.VENTE_FIND_BY_VEN_PRIXBRUT, query = "SELECT v FROM Vente v WHERE v.venPrixbrut = :venPrixbrut"),
    @NamedQuery(name = Vente.VENTE_FIND_BY_VEN_PRCTREMISE, query = "SELECT v FROM Vente v WHERE v.venPrctremise = :venPrctremise"),
    @NamedQuery(name = Vente.VENTE_FIND_BY_VEN_PRIXNET, query = "SELECT v FROM Vente v WHERE v.venPrixnet = :venPrixnet"),
    @NamedQuery(name = Vente.VENTE_FIND_MAX_ID, query = "select max(v.venId) from Vente v")
})
public class Vente implements Serializable {

  private static final long serialVersionUID = 1L;
  public static final String VENTE_FIND_ALL = "Vente.findAll";
  public static final String VENTE_FIND_BY_VEN_ID = "Vente.findByVenId";
  public static final String VENTE_FIND_BY_VEN_DTHEURE = "Vente.findByVenDtheure";
  public static final String VENTE_FIND_BY_VEN_PRIXBRUT = "Vente.findByVenPrixbrut";
  public static final String VENTE_FIND_BY_VEN_PRCTREMISE = "Vente.findByVenPrctremise";
  public static final String VENTE_FIND_BY_VEN_PRIXNET = "Vente.findByVenPrixnet";
  public static final String VENTE_FIND_MAX_ID = "Vente.find_max_id";
  public static final String VENTE_FIND_ALL_INVERSE = "vente.findAllInverse";

  @Id
  @Basic(optional = false)
  @Column(name = "VEN_ID")
  private Integer venId;

  @Basic(optional = false)
  @Column(name = "VEN_DTHEURE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date venDtheure;

  // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
  @Column(name = "VEN_PRIXBRUT")
  private Double venPrixbrut;

  @Column(name = "VEN_PRCTREMISE")
  private Double venPrctremise;

  @Column(name = "VEN_PRIXNET")
  private Double venPrixnet;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "vente")
  private List<Produitsvente> produitsventeList;

  @JoinColumn(name = "VEN_ID_TYPE_PAIEMENT", referencedColumnName = "TYP_ID")
  @ManyToOne
  private Typepaiement venIdTypePaiement;

  @JoinColumn(name = "VEN_ID_ETAT", referencedColumnName = "ETV_ID")
  @ManyToOne(optional = false)
  private Etatvente venIdEtat;

  public Vente(Integer venId, Date venDtheure, Double venPrixbrut, Double venPrctremise, Double venPrixnet, Etatvente venIdEtat) {
    this.venId = venId;
    this.venDtheure = venDtheure;
    this.venPrixbrut = venPrixbrut;
    this.venPrctremise = venPrctremise;
    this.venPrixnet = venPrixnet;
    this.venIdEtat = venIdEtat;
  }

  public Vente() {
  }

  public Vente(Integer venId) {
    this.venId = venId;
  }

  public Vente(Integer venId, Date venDtheure) {
    this.venId = venId;
    this.venDtheure = venDtheure;
  }

  public Integer getVenId() {
    return venId;
  }

  public void setVenId(Integer venId) {
    this.venId = venId;
  }

  public Date getVenDtheure() {
    return venDtheure;
  }

  public void setVenDtheure(Date venDtheure) {
    this.venDtheure = venDtheure;
  }

  public Double getVenPrixbrut() {
    return venPrixbrut;
  }

  public void setVenPrixbrut(Double venPrixbrut) {
    this.venPrixbrut = venPrixbrut;
  }

  public Double getVenPrctremise() {
    return venPrctremise;
  }

  public void setVenPrctremise(Double venPrctremise) {
    this.venPrctremise = venPrctremise;
  }

  public Double getVenPrixnet() {
    return venPrixnet;
  }

  public void setVenPrixnet(Double venPrixnet) {
    this.venPrixnet = venPrixnet;
  }

  @XmlTransient
  public List<Produitsvente> getProduitsventeList() {
    return produitsventeList;
  }

  public void setProduitsventeList(List<Produitsvente> produitsventeList) {
    this.produitsventeList = produitsventeList;
  }

  public Typepaiement getVenIdTypePaiement() {
    return venIdTypePaiement;
  }

  public void setVenIdTypePaiement(Typepaiement venIdTypePaiement) {
    this.venIdTypePaiement = venIdTypePaiement;
  }

  public Etatvente getVenIdEtat() {
    return venIdEtat;
  }

  public void setVenIdEtat(Etatvente venIdEtat) {
    this.venIdEtat = venIdEtat;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Vente)) return false;
    Vente vente = (Vente) o;
    return Objects.equals(getVenDtheure(), vente.getVenDtheure()) &&
        Objects.equals(getProduitsventeList(), vente.getProduitsventeList()) &&
        Objects.equals(getVenIdTypePaiement(), vente.getVenIdTypePaiement()) &&
        Objects.equals(getVenIdEtat(), vente.getVenIdEtat());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getVenDtheure(), getProduitsventeList(), getVenIdTypePaiement(), getVenIdEtat());
  }

  @Override
  public String toString() {
    return "Vente{" +
        "venId=" + venId +
        ", venIdEtat=" + venIdEtat +
        ", venDtheure=" + venDtheure +
        ", venIdTypePaiement=" + venIdTypePaiement +
        ", venPrixbrut=" + venPrixbrut +
        ", venPrctremise=" + venPrctremise +
        ", venPrixnet=" + venPrixnet +
        ", produitsventeList=" + produitsventeList +
        '}';
  }

}
