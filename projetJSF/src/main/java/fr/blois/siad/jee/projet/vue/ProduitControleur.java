package fr.blois.siad.jee.projet.vue;

import fr.blois.siad.jee.projet.entite.Produit;
import fr.blois.siad.jee.projet.services.ProduitService;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 *
 *
 */
@ViewScoped
@ManagedBean
public class ProduitControleur implements Serializable {

  private static final long serialVersionUID = 1L;

  @EJB
  private ProduitService produitService;

  public List<Produit> getProduitList() {
    return produitService.getProduitList();
  }

}
