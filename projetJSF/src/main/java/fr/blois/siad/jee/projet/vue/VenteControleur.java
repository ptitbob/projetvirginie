package fr.blois.siad.jee.projet.vue;

import com.sun.org.apache.regexp.internal.RE;
import fr.blois.siad.jee.projet.entite.Vente;
import fr.blois.siad.jee.projet.services.ProduitService;
import fr.blois.siad.jee.projet.services.VenteService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 *
 *
 */
@ManagedBean
@ViewScoped
public class VenteControleur {

  private static final long serialVersionUID = 1L;
  private static final String VENTE_ID = "venteId";

  @EJB
  private ProduitService produitService;

  @EJB
  private VenteService venteService;

  private Vente vente;

  @PostConstruct
  public void initialisation() {
      this.vente = venteService.nouvelleVente();
      
    //HttpServletRequest request = /* recupération de la requete HTTP */
      //  (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    //String sVenteId = request.getParameter(VENTE_ID);
    //if (sVenteId == null) {
      // Nouvelle vente
      //this.vente = venteService.nouvelleVente();

    //}
  }

  public Vente getVente() {
    return vente;
  }

  public void setVente(Vente vente) {
    this.vente = vente;
  }

  public void ajoutProduit(Integer venteId, Integer produitId) {
    venteService.ajouterProduit(vente, produitId);
  }

  public void appliquerRemise() {
    venteService.appliquerRemise(vente);
  }

  public String validerVente() {
    venteService.validerVente(vente);
    return "INDEX";
  }

  public String annulerVente() {
    venteService.annuleVente(vente);
    return "INDEX";
  }

  public List<Vente> getDerniereVenteList() {
    return venteService.getDerniereVenteList();
  }

}
